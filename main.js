// This data is for the F22 EECS 203 exam 1 regular time room assigmnents
// For other classes, exams, alternate or extended times, or semesters, this is
// not the data you want

// Pairs are [room, first uniquename assigned to room]
const ROOM_ASSIGNMENTS = [
    ["STAMPS", "AAA"],
    ["BBB 1670", "DEQ"],
    ["BBB 1690", "FCZ"],
    ["CHRYS 220", "GIN"],
    ["COOL G906", "JEW"],
    ["DOW 1010", "JUNB"],
    ["DOW 1013", "KSDB"],
    ["DOW 1014", "MBK"],
    ["DOW 1017", "NAA"],
    ["DOW 1018", "ORZ"],
    ["DOW 2150", "PIP"],
    ["DOW 2166", "RENG"],
    ["EECS 1003", "ROV"],
    ["EECS 1005", "SACHC"],
    ["EECS 1200", "SCHC"],
    ["EECS 1303", "SILB"],
    ["EECS 1500", "TERRC"],
    ["GGBL 1571", "VINOC"]
];

function getRoomAssignment(uniquename, assignments) {
    // Uniqnames are not uniq enough to handle emoji :(
    if (uniquename.includes('🍕')) {
        return "DOW 1013";
    }
    // Linear search; not fast, but fast to write
    for (let i = assignments.length - 1; i >= 0; i--) {
        const [room, firstUniquename] = assignments[i];
        if (uniquename.toLowerCase() >= firstUniquename.toLowerCase()) {
            return room;
        }
    }
    // Should always have returned, give back the first room just in case
    // the initial room somehow started too late
    return assignments[0][0];
}

function setRoomAssignment(room, roomInput) {
    roomInput.value = room;
}

function onChangeUniquename(uniquenameSoFar, roomInput) {
    if (uniquenameSoFar.length < 3) {
        setRoomAssignment("", roomInput);
    } else {
        const room = getRoomAssignment(uniquenameSoFar, ROOM_ASSIGNMENTS);
        setRoomAssignment(room, roomInput);
    }
}

const roomInput = document.querySelector("#room");
const uniquenameInput = document.querySelector("#uniquename");
uniquenameInput.addEventListener("input",
    () => onChangeUniquename(uniquenameInput.value, roomInput));
onChangeUniquename(uniquenameInput.value, roomInput);
